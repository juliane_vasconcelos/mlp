package mlp;

import holdout.Holdout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mlp.enumeration.WeightInitialization;
import output.util.MatrizConfusao;
import output.util.OutputMLP;
import preprocessamento.separacao_atributos_classes.LeitorDeBaseDeDados;
import preprocessamento.separacao_atributos_classes.objetos.Dado;

public class Mlp {

	private double learningRate;
	private int numberNeuronsInputLayer;
	private int numberNeuronsHiddenLayer;
	private int numberNeuronsOutputLayer;
	private double initialWeights[][];
	private double exitWeights[][];
	private double expectedAnswer[];
	private List<Double> inputLayer;
	private List<Double> hiddenLayer;
	private List<Double> outputLayer;
	private int epochs;
	private int frequency;
	private double limit;
	private double currentError;
	private double previousError;
	private OutputMLP outputInfo;

	public Mlp(double learningRate, int epochs, int frequency, double limit,
			int nNeuronsInputLayer, int nNeuronsHiddenLayer,
			int nNeuronsOutputLayer, WeightInitialization wi,
			double[][] weights1, double[][] weights2) throws IOException {
		this.learningRate = learningRate;
		this.epochs = epochs;
		this.frequency = frequency;
		this.limit = limit;
		this.currentError = 0;

		this.numberNeuronsInputLayer = nNeuronsInputLayer;
		this.numberNeuronsHiddenLayer = nNeuronsHiddenLayer;
		this.numberNeuronsOutputLayer = nNeuronsOutputLayer;

		this.initialWeights = new double[numberNeuronsInputLayer][numberNeuronsHiddenLayer];
		this.exitWeights = new double[numberNeuronsHiddenLayer][numberNeuronsOutputLayer];
		this.expectedAnswer = new double[numberNeuronsOutputLayer];

		outputInfo = new OutputMLP();
		initializeWeights(wi, weights1, weights2);
	}

	private void initializeLayers() {
		this.inputLayer = new ArrayList<Double>();
		this.hiddenLayer = new ArrayList<Double>();
		this.outputLayer = new ArrayList<Double>();

		for (int i = 0; i < numberNeuronsHiddenLayer; i++) {
			hiddenLayer.add(0.0);
		}

		for (int i = 0; i < numberNeuronsOutputLayer; i++) {
			outputLayer.add(0.0);
		}
	}

	public void useMLP(List<Dado> dataBase, double[][] weights1,
			double[][] weights2) throws IOException {

		initializeWeights(WeightInitialization.MANUAL, weights1, weights2);

		int[][] result = new int[dataBase.size()][2];
		// method is only called once, cause the net has already been trained
		// data is sent to network
		result = passThroughNetwork(dataBase, true, false);

		MatrizConfusao matrizConfusao = new MatrizConfusao(10, result, 1);

		matrizConfusao.escreveMatriz();
	}

	public void validateMLP(List<Dado> dataBase) {

		// validation dataset is sent to network
		passThroughNetwork(dataBase, false, false);

	}

	public void storeError() {
		this.previousError = this.currentError;
		this.currentError = 0;
	}

	public void trainMLP(List<Dado> dataBase, List<Dado> validationDataset)
			throws IOException {
		int localCounter = 0;
		int numberOfTries = 0;
		for (int a = 0; a < epochs
				&& !numberOfTriesExceeded(numberOfTries, previousError,
						currentError); a++) {
			System.out.println("Epoch " + (a + 1));
			passThroughNetwork(dataBase, false, true);

			localCounter++;
			if (localCounter == frequency) {
				// every time the defined frequency is reached, the training
				// error, and the set of weights (for the current epoch) are
				// stored
				localCounter = 0;
				double trainingError = currentError;

				outputInfo.escrevePeso(a + 1, initialWeights, exitWeights);

				// also, validation is called and its error stored
				validateMLP(validationDataset);
				outputInfo.escreveErro(a + 1, trainingError, currentError);

				//changeLearningRate();
			}
		}
	}

	public void changeLearningRate() {
		// learning rate changed to 90% of its original value
		learningRate *= 0.9;
	}

	public int verifyErrorVariation(int performed, double previousError,
			double workingError) {
		// if the error keeps getting lower, then we ignore it
		if (workingError < previousError) {
			return 0;
		}// but in a scenario where it is growing, we consider that epoch a
			// failed attempt
		return ++performed;
	}

	public boolean numberOfTriesExceeded(int performed, double previousError,
			double workingError) {
		// the network will stop after more than 10 unsuccessful attempts to
		// learn
		if (verifyErrorVariation(performed, previousError, workingError) < 10) {
			System.out.println("Attempts " + performed);
			return false;
		}
		System.out.println("Number of attempts exceeded: " + performed
				+ ". Current scenario (" + workingError + " > " + previousError
				+ " )");
		// otherwise, the network keeps on the adjustment procedure
		return true;
	}

	public static void main(String [] args) throws IOException{
				LeitorDeBaseDeDados leitor = new LeitorDeBaseDeDados();
				Holdout holdout = new Holdout();
				
				List<Dado> testDataset = holdout.getListaTeste();
				
		 		
		 		Mlp mlp = new Mlp(0.5, 10000, 25, 0.0001, 57, 10, 4, WeightInitialization.RANDOM,null,null);
		 		
		 		LeitorPesos lp = new LeitorPesos("C:/Users/Felipe/Desktop/classes/IA/project/testResults/mlp/10neuronios/treinaMLPPesosRandomicosCom10NeuroniosTaxaAprendizadoMaiorPesosEpoca-5660.txt");
				
				double[][] weights1 = lp.retornaPesosInput();
				double[][] weights2 = lp.retornaPesosOutput();
				
		 		mlp.useMLP(testDataset, weights1, weights2);
		 		
		 	}
	
	public int[][] passThroughNetwork(List<Dado> dataBase, boolean isNetReady,
			boolean updateWeights) {
		// limiar tem que para a execucao
		int[][] result = new int[dataBase.size()][2];
		// TODO colocar como 'main' e tirar a parte de alteracao de pesos
		// criando um metodo especifico para o treinamento
		initializeLayers();

		storeError();

		int counter = 0;

		for (Dado data : dataBase) {

			this.expectedAnswer = generateExpectedAnswer(data.getClasse());

			initializeLayers();
			// for each pair of training data - input, expected output
			double[] originalNumber = data.getAttributes();
			// running through the properties
			for (int i = 0; i < originalNumber.length; i++) {
				// setting the values of the input layer neurons
				this.inputLayer.add(originalNumber[i]);

			}
			// forwarding the information to the hidden layer
			treatAndSumReceivedData(inputLayer, hiddenLayer, initialWeights);

			// applies the activation function over the whole hidden layer
			calculateActvationFunctionForLayer(hiddenLayer);

			// forwarding the information to the output layer
			treatAndSumReceivedData(hiddenLayer, outputLayer, exitWeights);

			// applies the activation function over the whole output layer
			calculateActvationFunctionForLayer(outputLayer);

			if (!isNetReady)
				improveAccuracy(updateWeights);

			/*
			 * String tempOutput = new String(); for(int i = 0; i <
			 * outputLayer.size();i++){ tempOutput += ", " + outputLayer.get(i);
			 * } System.out.println("dado "+expectedAnswer[0] + ","+
			 * expectedAnswer[1] + ","+ expectedAnswer[2] + ","+
			 * expectedAnswer[3] + ","+ " saida "+tempOutput);
			 */
			// storing the result, along with the expected value in the output
			// matrix
			result[counter][0] = data.getClasse();
			result[counter][1] = constructFinalNumber();

			//System.out.println(result[counter][0]+","+result[counter][1]);
			counter++;
		}

		return result;

	}

	// responsible for generating the number based on the output layer
	private int constructFinalNumber() {

		String output = "";

		for (int i = 0; i < outputLayer.size(); i++) {
			output += Math.round(outputLayer.get(i));
		}
		int result = Integer.parseInt(output, 2);

		return result;
	}

	private double[] generateExpectedAnswer(int neuronClass) {
		// TODO Auto-generated method stub
		String[] expectedInChar = Integer.toBinaryString(neuronClass)
				.toString().split("");
		double[] expectedInDouble = new double[4];
		int i2 = 0;
		boolean hasExecuted = false;
		for (int i = 0; i < expectedInDouble.length; i++) {
			if (!hasExecuted && expectedInChar.length < 4) {
				hasExecuted = true;
				expectedInDouble[0] = 0;
				i++;
				if (expectedInChar.length < 3) {
					expectedInDouble[1] = 0;
					i++;
					if (expectedInChar.length < 2) {
						expectedInDouble[2] = 0;
						i++;
					}
				}
			}
			expectedInDouble[i] = Double.parseDouble(expectedInChar[i2]);
			i2++;
		}
		return expectedInDouble;
	}

	private void improveAccuracy(boolean updateWeights) {
		// outerCorrectionRate is passed by reference, so that the error and the
		// correction rate can be returned
		double outerCorrectionRate[][] = new double[hiddenLayer.size()][outputLayer
				.size()];
		double outerError[] = calculateError(outerCorrectionRate);

		// innerCorrectionRate is passed by reference, so that the error and the
		// correction rate can be returned
		double innerCorrectionRate[][] = new double[inputLayer.size()][hiddenLayer
				.size()];
		backPropagation(outerError, innerCorrectionRate);

		if (updateWeights)
			updateWeights(outerCorrectionRate, innerCorrectionRate);
	}

	private void treatAndSumReceivedData(List<Double> iLayer,
			List<Double> oLayer, double[][] weightMatrix) {

		for (int i = 0; i < iLayer.size(); i++) {
			for (int j = 0; j < oLayer.size(); j++) {
				oLayer.set(j,
						oLayer.get(j) + (weightMatrix[i][j] * iLayer.get(i)));
			}
		}
	}

	private void calculateActvationFunctionForLayer(List<Double> layer) {
		// receives an entire layer and applies the activation function (by
		// calling another method) to all of the neurons
		for (int i = 0; i < layer.size(); i++) {
			layer.set(i, calculateActvationFunction(layer.get(i)));
		}
	}

	private double calculateActvationFunction(double neuron) {
		// apply activation function on the received neuron. Choice, sigmoid
		// function
		return 1 / (1 + Math.exp(-neuron));
	}

	public void initializeWeights(WeightInitialization wi, double[][] weights1,
			double[][] weights2) {
		// the weights can be initialized randomly, with 0's or with user chosen
		// values
		if (wi.equals(WeightInitialization.RANDOM)) {
			Random rd = new Random();

			for (int i = 0; i < this.initialWeights.length; i++) {
				for (int j = 0; j < this.initialWeights[i].length; j++) {
					this.initialWeights[i][j] = rd.nextDouble();
				}
			}

			for (int i = 0; i < this.exitWeights.length; i++) {
				for (int j = 0; j < this.exitWeights[i].length; j++) {
					this.exitWeights[i][j] = rd.nextDouble();
				}
			}

		} else if (wi.equals(WeightInitialization.ZERO)) {
			for (int i = 0; i < this.initialWeights.length; i++) {
				for (int j = 0; j < this.initialWeights[i].length; j++) {
					this.initialWeights[i][j] = 0.0;
				}
			}

			for (int i = 0; i < this.exitWeights.length; i++) {
				for (int j = 0; j < this.exitWeights[i].length; j++) {
					this.exitWeights[i][j] = 0.0;
				}
			}
		} else {// this case is only used in the test phase, so that the user
				// can simulate the exact same architecture he has tried before
				// in the best training epoch
			if (weights1 != null && weights2 != null) {
				this.initialWeights = weights1;
				this.exitWeights = weights2;
			}
		}
	}

	// define accuracy level
	private double[] calculateError(double[][] correctionRate) {
		// TODO Auto-generated method stub
		double error[] = new double[outputLayer.size()];

		for (int i = 0; i < outputLayer.size(); i++) {
			// if(!outputLayer.get(i).equals(expectedAnswer[i])){
			// System.out.println(expectedAnswer[i]+" - "+outputLayer.get(i));
			if (Math.abs(expectedAnswer[i] - outputLayer.get(i)) > limit) {
				error[i] = (expectedAnswer[i] - outputLayer.get(i))
						* (outputLayer.get(i) * (1 - outputLayer.get(i)));
				currentError += Math.pow((expectedAnswer[i] - outputLayer.get(i)), 2);
				for (int j = 0; j < hiddenLayer.size(); j++) {
					correctionRate[j][i] = learningRate * error[i]
							* hiddenLayer.get(j);
				}
			}
		}
		// TODO calculate bias correction

		return error;
	}

	public double[] backPropagation(double[] outerError,
			double[][] correctionRate) {
		double innerError[] = new double[hiddenLayer.size()];
		for (int i = 0; i < hiddenLayer.size(); i++) {
			for (int j = 0; j < outputLayer.size(); j++) {
				innerError[i] += outerError[j] * exitWeights[i][j];
			}
			innerError[i] *= (hiddenLayer.get(i) * (1 - hiddenLayer.get(i)));
			for (int k = 0; k < inputLayer.size(); k++) {
				correctionRate[k][i] = learningRate * innerError[i]
						* inputLayer.get(k);
			}

		}

		// TODO calculate bias correction

		return innerError;
	}

	public void updateWeights(double[][] outerCorrectionRate,
			double[][] innerCorrectionRate) {

		for (int i = 0; i < exitWeights.length; i++) {
			for (int j = 0; j < exitWeights[i].length; j++) {
				exitWeights[i][j] += outerCorrectionRate[i][j];
			}
		}

		for (int i = 0; i < initialWeights.length; i++) {
			for (int j = 0; j < initialWeights[i].length; j++) {
				initialWeights[i][j] += innerCorrectionRate[i][j];
			}
		}
	}

}
